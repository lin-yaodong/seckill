package com.lyd.seckill.serviceimpl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lyd.seckill.mapper.SeckillGoodsMapper;
import com.lyd.seckill.pojo.SeckillGoods;
import com.lyd.seckill.service.ISeckillGoodsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 */
@Service
public class SeckillGoodsServiceImpl extends ServiceImpl<SeckillGoodsMapper, SeckillGoods> implements ISeckillGoodsService {

}
