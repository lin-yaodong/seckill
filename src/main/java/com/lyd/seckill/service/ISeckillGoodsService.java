package com.lyd.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyd.seckill.pojo.SeckillGoods;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface ISeckillGoodsService extends IService<SeckillGoods> {

}
