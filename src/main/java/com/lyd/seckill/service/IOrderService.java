package com.lyd.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyd.seckill.pojo.Order;
import com.lyd.seckill.pojo.User;
import com.lyd.seckill.vo.GoodsVo;
import com.lyd.seckill.vo.OrderDetailVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface IOrderService extends IService<Order> {


    /**
     * 秒杀
     * @param user
     * @param goods
     * @return
     */
    Order seckill(User user, GoodsVo goods);


    /**
     * 订单详情
     * @param orderId
     * @return
     */
    OrderDetailVo detail(Long orderId);


    /**
     * 获取秒杀地址
     * @param user
     * @param goodsId
     * @return
     */
    String createPath(User user, Long goodsId);


    /**
     * 校验秒杀地址
     * @param user
     * @param goodsId
     * @param path
     * @return
     */
    boolean checkPath(User user, Long goodsId, String path);


    /**
     * 校验验证码
     * @param user
     * @param goodsId
     * @param captcha
     * @return
     */
    boolean checkCaptcha(User user, Long goodsId, String captcha);

}
