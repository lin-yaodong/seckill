package com.lyd.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyd.seckill.pojo.SeckillOrder;

/**
 * <p>
 *  Mapper 接口
 *
 *
 */
public interface SeckillOrderMapper extends BaseMapper<SeckillOrder> {

}
