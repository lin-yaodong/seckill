package com.lyd.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyd.seckill.pojo.User;

/**
 * <p>
 *  Mapper 接口
 *
 */
public interface UserMapper extends BaseMapper<User> {

}
