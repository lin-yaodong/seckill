package com.lyd.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyd.seckill.pojo.Order;

/**
 * <p>
 *  Mapper 接口
 *
 */
public interface OrderMapper extends BaseMapper<Order> {

}
