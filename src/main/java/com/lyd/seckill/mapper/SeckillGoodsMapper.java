package com.lyd.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyd.seckill.pojo.SeckillGoods;


/**
 * <p>
 *  Mapper 接口
 *
 */
public interface SeckillGoodsMapper extends BaseMapper<SeckillGoods> {

}
