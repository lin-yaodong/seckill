package com.lyd.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyd.seckill.pojo.Goods;
import com.lyd.seckill.vo.GoodsVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 *
 */
public interface GoodsMapper extends BaseMapper<Goods> {


    /**
     * 获取商品列表
     * @return
     */
    List<GoodsVo> findGoodsVo();


    /**
     * 获取商品详情
     * @return
     * @param goodsId
     */
    GoodsVo findGoodsVoByGoodsId(Long goodsId);
}
