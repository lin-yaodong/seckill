# seckill

#### 介绍
基于springboot、redis、rabbitmq开发秒杀商城项目，本项目重点在实现后端秒杀功能，前端只写了几个简单的页面。仅供参考交流。

秒杀主要解决两个问题，并发读和并发写。并发读的核心优化理念就是尽量减少用户到服务端来读数据，或者让用户读更少的数据；并发写也一样，我们要在数据库层做一个独立的库，来做特殊的处理

#### 软件架构

- 前端：Thymeleaf模板、Bootstrap、Jquery
- 后端：Spring Boot、Mybatis-Plus、Lombok、Redis、RabbitMQ、JMeter压测工具
- 开发工具：IDEA、Git、Maven、FinalShell、VMware、Redis Desktop Manager



#### 秒杀方案

 **分布式会话：** 

1.用户登录

- 设计数据库
- 明文密码进行二次MD5加密
- 参数校验+全局异常处理

2.共享Session


- SpringSession
- Redis

 **功能开发：** 

- 商品列表
- 商品详情
- 秒杀
- 订单详情

 **系统压测：** 


1. JMeter
1. 自定义变量模拟多用户
1. JMeter命令行的使用
1. 正式压测

- 商品列表
- 秒杀

 **页面优化：** 

- 页面缓存+URL缓存+对象缓存
- 页面静态化，前后端分离
- 静态资源优化
- CDN优化

 **接口优化：** 

1. Redis预减库存减少数据库的访问
1. 内存标记减少Redis的访问
1. RabbitMQ异步下单


- SpringBoot整合RabbitMQ
- 交换机

 **安全优化：** 

- 秒杀接口地址隐藏
- 算术验证码
- 对接口限流，防刷



#### 特别鸣谢


- 乐字节：[项目链接](https://www.bilibili.com/video/BV1sf4y1L7KE?p=1&vd_source=e2d884b776f8d80a93971ef35e37d3cb)
- mybatis-plus：[官网](https://baomidou.com/pages/24112f/)
- rabbitmq：[官网](https://www.rabbitmq.com/)
- redis：[官网](https://www.redis.net.cn/)



